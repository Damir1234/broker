from sqlalchemy import *
from src.config import ROLE_GUEST
from src.config import START_BALANCE
from src.config import START_RATING

meta = MetaData()

# entity
users = Table(
    'users', meta,
    Column('id', Integer, primary_key=True),
    Column('bt_chat_id', Unicode, nullable=False),  # bt- bot
    Column('bt_username', Unicode, nullable=False, unique=True),
    Column('balance', Integer, default=START_BALANCE),
    Column('role', Integer, default=ROLE_GUEST),
    sqlite_autoincrement=True
)

# entity
happenings = Table(
    'happenings', meta,
    Column('id', Integer, primary_key=True),
    Column('owner_id', ForeignKey('users.id')),
    Column('description', Unicode, nullable=False),
    Column('is_published', Boolean, default=False),
    sqlite_autoincrement=True
)

# entity
outcome_lines = Table(
    'outcome_lines', meta,
    Column('id', Integer, primary_key=True),
    Column('happening_id', ForeignKey('happenings.id')),
    Column('description', Unicode, nullable=False),
    Column('is_published', Boolean, default=False),
    sqlite_autoincrement=True
)

# values
outcomes = Table(
    'outcomes', meta,
    Column('id', Integer, primary_key=True), # need for sqlalchemy
    Column('outcome_line_id', ForeignKey('outcome_lines.id')),
    Column('description', Unicode, nullable=False),
    Column('rating_bet', Float, default=START_RATING),
    sqlite_autoincrement = True
)


def upgrade(migrate_engine):
    meta.bind = migrate_engine
    users.create()
    happenings.create()
    outcome_lines.create()
    outcomes.create()


def downgrade(migrate_engine):
    meta.bind = migrate_engine
    outcomes.drop()
    outcome_lines.drop()
    happenings.drop()
    users.drop()
