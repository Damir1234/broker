"""
Routing clients request
"""

from src import bot
from src import models
from src.models import users
from telebot import types
import shelve
from src.config import SHELVE_FILE
import abc


@models.local_session
@bot.message_handler(commands=['start'])
def registry_client(m):
    chat = bot.get_chat(m.chat.id)
    with users.Repository() as repo:
        user = repo.find_by_name(chat.username)
        if user is None:
            user = users.factory.create(chat.id, chat.username)
            repo.save(user)
            answer = 'Successful add username: %s' % (user.name,)
        else:
            answer = 'Already yet'
    bot.send_message(chat.id, answer)

    caretaker = CaretakerRoute()
    router = RouteManager(chat.username)
    router.print_message(chat.id)
    caretaker.save_state(chat.id, chat.username, state=router.create_momento())



@bot.message_handler(content_types=["text"])
@models.local_session
def repeat_all_messages(m):
    chat = bot.get_chat(m.chat.id)
    router = RouteManager(chat.username)
    caretaker = CaretakerRoute()

    momento = caretaker.find_state(chat.id, chat.username)
    router.change_momento(momento)

    router.step(m.text)
    router.print_message(chat.id)
    caretaker.save_state(chat.id, chat.username, state=router.create_momento())


class CaretakerRoute(object):
    def __init__(self):
        pass

    def find_state(self, *keys):
        key = ''.join(map(str, keys))
        with shelve.open(SHELVE_FILE) as db:
            return db.get(key, MomentoRouteManager(MainState, None))

    def save_state(self, *keys, state=None):
        key = ''.join(map(str, keys))
        with shelve.open(SHELVE_FILE) as db:
            if state is None:
                del db[key]
            else:
                db[key] = state


class MomentoRouteManager(object):
    def __init__(self, state_name, params):
        self.__state_name = state_name
        self.__params = params

    @property
    def state_name(self):
        return self.__state_name

    @property
    def params(self):
        return self.__params


class RouteManager(object):
    def __init__(self, username):
        self.user = users.Repository().find_by_name(username)
        self.state = MainState(self)

    def change_momento(self, momento=None):
        if(momento is not None):
            self.state = momento.state_name(self)
            self.state.set_inner_state(momento.params)

    def create_momento(self):
        return MomentoRouteManager(self.state.__class__, self.state.get_inner_state())

    def step(self, message):
        self.state.handle(message)

    def print_message(self, chat_id):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.row(*self.state.get_keyboard_row())
        text_message = self.state.get_message()
        bot.send_message(chat_id, text_message, reply_markup=markup)

    def set_state(self, class_state):
        self.state = class_state(self)


class RouteState(metaclass=abc.ABCMeta):
    def __init__(self, context):
        self.context = context

    # for getting inner state
    def get_inner_state(self):
        return None

    # for setting inner state
    def set_inner_state(self, params):
        pass

    def get_message(self):
        mes = []
        for k, v in zip(self.params.keys(), self.params.values()):
            mes.append("%s. %s" % (k, v.get('message')))
        mes.sort()
        return '\n'.join(mes)

    def get_keyboard_row(self):
        lst = list(self.params.keys())
        lst.sort()
        return lst

    @property
    @abc.abstractmethod
    def params(self):
        raise NotImplementedError('Please implement this property')

    def handle(self, key):
        if key in self.params:
            next_state = self.params[key].get('next_state')
            self.context.set_state(next_state)
        else:
            raise NotImplementedError('Encorrect message') # TODO set other exception


class MainState(RouteState):
    def get_message(self):
        return 'Главное меню:\n' + super(MainState, self).get_message()

    @RouteState.params.getter
    def params(self):
        return {
            '1': {
                'message': 'Получить выйгрыш',
                'next_state': GettingCashState
            },
            '2': {
                'message': 'Сделать ставку',
                'next_state': StartCreateBetState
            },
            '3': {
                'message': 'Создать игровое событие',
                'next_state': StartCreateHappeningState
            },
            '4': {
                'message': 'Установить исходы игрового события',
                'next_state': StartSetOutcomesState
            },
        }

class GettingCashState(RouteState):
    @RouteState.params.getter
    def params(self):
        return {
            '1': {
                'message': 'No implements',
                'next_state': MainState
            },
        }


class StartCreateBetState(RouteState):
    @RouteState.params.getter
    def params(self):
        return {
            '1': {
                'message': 'No implements',
                'next_state': MainState
            },
        }


class StartCreateHappeningState(RouteState):
    @RouteState.params.getter
    def params(self):
        return {
            '1': {
                'message': 'No implements',
                'next_state': MainState
            },
        }


class StartSetOutcomesState(RouteState):
    @RouteState.params.getter
    def params(self):
        return {
            '1': {
                'message': 'No implements',
                'next_state': MainState
            },
        }
