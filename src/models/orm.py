from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from src import db_engine
from src.config import ROLE_GUEST
from src.config import START_BALANCE
from src.config import START_RATING

Base = declarative_base(bind=db_engine)


class User(Base):
    __tablename__ = 'users'
    id = Column('id', Integer, primary_key=True)
    chat_id = Column('bt_chat_id', Unicode, nullable=False)
    name = Column('bt_username', Unicode, nullable=False, unique=True)
    balance = Column('balance', Integer, default=START_BALANCE)
    role = Column('role', Integer, default=ROLE_GUEST)


class Happening(Base):
    __tablename__ = 'happenings'
    id = Column('id', Integer, primary_key=True)

    # owner_id = Column('user_id', Integer, nullable=False)
    owner_id = Column('owner_id', ForeignKey('users.id'))
    owner = relationship('User', uselist=False)

    desc = Column('description', Unicode, nullable=False)
    is_published = Column('is_published', Boolean, default=False)
    outcome_lines = relationship('OutcomeLine')


class OutcomeLine(Base):
    __tablename__ = 'outcome_lines'
    id = Column('id', Integer, primary_key=True)

    # happening_id = Column('happening_id', Integer, nullable=False)
    happening_id = Column('happening_id', ForeignKey('happenings.id'))

    desc = Column('description', Unicode, nullable=False)
    is_published = Column('is_published', Boolean, default=False)

    outcomes = relationship('Outcome')


class Outcome(Base):
    __tablename__ = 'outcomes'
    id = Column('id', Integer, primary_key=True)

    # line_id = Column('outcome_line_id', Integer, nullable=False)
    line_id = Column('outcome_line_id', ForeignKey('outcome_lines.id'))

    desc = Column('description', Unicode, nullable=False)
    rating_bet = Column('rating_bet', Float, default=START_RATING)


# class Bet(Base):
#     __tablename__ = 'bets'
#     id = Column('id', Integer, primary_key=True)
#     amount = Column('amount', Integer, nullable=0)
#     user_id = Column('user_id', ForeignKey('users.id'))
#     outcome_id = Column('outcome_id', ForeignKey('outcomes.id'))
#
