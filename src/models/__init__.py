from src import db_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from time import time

Base = declarative_base(bind=db_engine)

session_factory = sessionmaker(bind=db_engine)
Session = scoped_session(session_factory)


def local_session(f):
    """
    see http://docs.sqlalchemy.org/en/rel_1_0/__orm/contextual.html#using-custom-created-scopes
    :param f:
    :return:
    """

    def decorate(*args, **kwargs):
        identity = '%s %f' % (f.__name__, time())
        def get_current_request():
            return identity

        global Session
        Session = scoped_session(session_factory, scopefunc=get_current_request)
        res = f(*args, **kwargs)
        Session.remove()
        return res

    return decorate
