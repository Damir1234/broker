from src.models import Session
from src.models import orm
from src.models.users import User


class Happening(object):
    def __init__(self, happening_orm):
        self.__orm = happening_orm

    def orm(self):
        return self.__orm

    @property
    def desc(self):
        return self.__orm.desc

    @property
    def owner(self):
        u_orm = self.__orm.owner
        return User(u_orm)

    def add_line(self, desc_line):
        line = orm.OutcomeLine(desc=desc_line)
        self.__orm.outcome_lines.append(line)
        return OutcomeLine(line)


class OutcomeLine(object):
    def __init__(self, line_orm):
        self.__orm = line_orm

    @property
    def desc(self):
        return self.__orm.desc

    def add_outcome(self, desc_outcome):
        outcome = orm.Outcome(desc=desc_outcome)
        self.__orm.outcomes.append(outcome)
        return Outcome(outcome)


class Outcome(object):
    def __init__(self, outcome_orm):
        self.__orm = outcome_orm

    @property
    def desc(self):
        return self.__orm.desc


class Factory(object):
    @staticmethod
    def create(user, desc):
        """
        :type user: User
        """
        hap_orm = orm.Happening(desc=desc, owner=user.orm())
        return Happening(hap_orm)


class Repository(object):
    def __init__(self):
        self.__session = Session()

    def __del__(self):
        self.__session.commit()

    def __enter__(self):
        return self

    def __exit__(self, *ext):
        self.__session.commit()
        return False

    def save(self, happening):
        """
        :type happening: Happening
        """
        self.__session.add(happening.orm())
        self.__session.commit()
