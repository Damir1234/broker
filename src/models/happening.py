from uuid import uuid4

from sqlalchemy import *
from sqlalchemy.orm import mapper

from src.config import START_RATING
from src.models import Session

meta = MetaData()
__happenings_table = Table('happenings', meta,
                           Column('id', String(36), primary_key=True),
                           Column('owner_id', Integer, nullable=False),
                           Column('description', Unicode, nullable=False),
                           Column('is_published', Boolean, default=False),
                           sqlite_autoincrement=True
                           )


class Happening(object):
    def __init__(self, user, description, lines=None):
        """

        :type user: _User
        """
        self.id = str(uuid4())
        self.desc = description
        self.owner = user
        self.owner_id = user.id
        self.outcome_lines = lines if lines is not None else []

    def add_outcome_line(self, description):
        line = _OutcomeLine(self, description)
        self.outcome_lines.append(line)

    def get_line(self, id):
        lines = filter(lambda l: l.id==id, self.outcome_lines)
        return next(lines)


mapper(Happening, __happenings_table, properties={
    'desc': __happenings_table.c.description,
})
__outcome_lines_table = Table('outcome_lines', meta,
                              Column('id', String(36), primary_key=True),
                              Column('happening_id', String(36), nullable=False),
                              Column('description', Unicode, nullable=False),
                              Column('is_published', Boolean, default=False),
                              sqlite_autoincrement=True
                              )


class _OutcomeLine(object):
    def __init__(self, happening, description, outcomes=None):
        """

        :type happening: Happening
        """
        self.id = str(uuid4())
        self.happening_id = happening.id
        self.desc = description
        self.outcomes = outcomes if outcomes is not None else []

    def add_outcome(self, description):
        outcome = _Outcome(self, description)
        self.outcomes.append(outcome)


mapper(_OutcomeLine, __outcome_lines_table, properties={
    'desc': __outcome_lines_table.c.description,
}) #, include_properties=['outcomes']
__outcomes_table = Table('outcomes', meta,
                         Column('id', Integer, primary_key=True),  # need for sqlalchemy
                         Column('outcome_line_id', String(36), nullable=False),
                         Column('description', Unicode, nullable=False),
                         Column('rating_bet', Float, default=START_RATING),
                         sqlite_autoincrement=True
                         )


class _Outcome(object):
    def __init__(self, outcome_line, description):
        """

        :type outcome_line: _OutcomeLine
        """
        self.outcome_line_id = outcome_line.id
        self.desc = description


mapper(_Outcome, __outcomes_table, properties={
    'desc': __outcomes_table.c.description
})


class Repository(object):
    def __init__(self):
        self.__session = Session()

    def __del__(self):
        self.__session.commit()

    def __enter__(self):
        return self

    def __exit__(self, *ext):
        self.__session.commit()
        return False

    def find_by_id(self, user, id):
        happening = self.__session.query(Happening).filter_by(id=id, owner_id=user.id).first()
        return self.__build_happening(happening)

    def __build_happening(self, hap):
        """

        :type hap: Happening
        """
        hap.outcome_lines = self.__session.query(_OutcomeLine).filter_by(happening_id=hap.id).all()
        for outcome in self.__session.query(_Outcome).join(_OutcomeLine, _Outcome.outcome_line_id==_OutcomeLine.id).\
                filter(_OutcomeLine.happening_id==hap.id).all():
            hap.get_line(outcome.outcome_line_id).add_outcome(outcome)
        return hap



    def save(self, happening):
        """

        :type happening: Happening
        """
        hap_list = [happening]
        hap_list.extend(happening.outcome_lines)
        for outcome_line in happening.outcome_lines:
            hap_list.extend(outcome_line.outcomes)
        self.__session.add_all(hap_list)

