# __init__.py
__version__ = '0.1'

from src import config
import telebot
from sqlalchemy import create_engine

# Create bot
bot = telebot.TeleBot(config.TELEGRAM_BOT_TOKEN)

db_engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
# Execute SQL: `PRAGMA foreign_keys=ON` for support foreign key on sqlite

from src import route
from src import models


def main():
    bot.polling(none_stop=True)  # Cyclic request telegram server
