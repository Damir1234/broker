import os

basedir = os.path.abspath(os.path.dirname(__file__))

# Secret string of registry on bot @BotFather
TELEGRAM_BOT_TOKEN = '269532043:AAEBXUjmy5Os_KCz8BGp3AXErp1sFlpClxY'

SQLALCHEMY_MIGRATE_REPO = os.path.join(os.path.dirname(basedir), 'db_repository')
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(SQLALCHEMY_MIGRATE_REPO, 'app.db')

SHELVE_FILE = os.path.join(SQLALCHEMY_MIGRATE_REPO, 'shelve.db')


ROLE_GUEST = 0
ROLE_ADMIN = 10

START_BALANCE = 100
START_RATING = 1

