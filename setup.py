#!/usr/bin/env python

"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

from setuptools import setup, find_packages

setup(
    name='broker',

    version='0.1.0',

    description='A example project for game',
    long_description='long description',

    author='Universal',
    author_email='damirm@list.ru',

    license='MIT',

    classifiers=[
        'Programming Language :: Python :: 3.5',
    ],

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

    install_requires=['SQLAlchemy', 'pytelegrambotapi', 'SQLAlchemy-migrate'],

    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
)
